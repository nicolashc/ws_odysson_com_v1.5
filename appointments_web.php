
<?php

header('Access-Control-Allow-Origin: *');

include_once '../private_dev/db/db_wrapper.php';

$db = new DB_Wrapper();
$json = 0;


session_start();








//~ --------------------------------------------------------------------------------2017 WEB Appointments CRUD------------------------------------------------------------------------
	 

	 
 if(!empty($_POST["updateWebAppointmentJSON"])) {

    $json = $_POST["updateWebAppointmentJSON"];
	$data = json_decode($json); 
	$status = 200;
	
	
	try{
		$result = $db->updateWebAppointmentWordpress($data);
		 $ref = $result;
		echo json_encode(array("response"=>$status,"ref"=>$data->ref));
		
	} 
	catch(Exception $e){

		 echo json_encode(array("response"=>$e->getMessage()));
		 exit();
	} 


} 



else if(!empty($_POST["cancelappointmentJSON"])){
    $json = $_POST["cancelappointmentJSON"];
    $data = json_decode($json); //Decode JSON 
	
    $status = 200;
    
     if(!$db->cancelAppointmentWordpress($data)){
        $status = 500;
     }
    
    http_response_code($status);
    echo json_encode(array("response"=>$status));

 
} 




else if(!empty($_POST["readWebAppointmentsJSON"]) ) {
 
    $json = $_POST["readWebAppointmentsJSON"];
    $data = json_decode($json); //Decode JSON 
	echo json_encode($db->readWebSlotsWordpress($data));
} 





//~ --------------------------------------------------------------------------------END OF 2017 Appointments CRUD------------------------------------------------------------------------









else {
    http_response_code(405);
    echo json_encode(array("response"=>"unknown request\n"));

}
